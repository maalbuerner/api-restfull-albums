'use strict'

// var mongoose = require('mongoose');
var mysql = require('mysql');
var app = require('./app');
var port = process.env.PORT || 3268;

// app.listen(3268, function(){
// 	console.log('API Rest funcionando en http://localhost:3268');
// });
var con = mysql.createConnection({
  host: "localhost",
  user: "pepe",
  password: "",
  database: "appalbum"
});

// mongoose.connect('mongodb://localhost:27017/albums', (err, res) => {
// 	if(err){
// 		throw err;
// 	} else {
// 		console.log("Base de datos online... OK");

// 		app.listen(port, ()=>{
// 			console.log('API RESTFul de albums escuchando...');
// 		})
// 	}
// });

con.connect(function(err) {
  if (err) throw err;
  else
  {
	console.log("Base de datos funcionando correctamente...OK");
	var album = require('./models/album');
	var image = require('./models/image');

	// console.log(image.createTable());

	con.query(album.createTable(), function (err, result) {
	    if (err) 
	    	throw err;
	    else
	    {
	    	console.log("Tabla album creada correctamente.");
	    	con.query(image.createTable(), (err, result)=>{
			    if (err) 
			    	throw err;
			    else
			    {
			    	console.log('Tabla image creada correctamente.');
			    }
	    	});
		}
  	});

	app.listen(port, ()=>{
		console.log(`API RESTFul escuchando por http://localhost:${port}`);
	});
  }
});