'use strict'

var path = require('path');
var image = require('../models/image');
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "pepe",
  password: "",
  database: "appalbum"
});


function getImage(req, res)
{
	var imageId = req.params.id;

	con.query(image.getById(imageId), (err, image) =>{
		if(err)
		{
			res.status(500).send({message: 'Error en la peticion'})
		}
		else
		{
			if(!image)
			{
				res.status(404).send({message: 'El image no exite.'});
			}
			else
			{
				res.status(200).send(image[0]);
			}
		}
	});
}

function getImages(req, res)
{
	var id_album = req.params.id_album;

	var query = image.getImages();

	if(id_album)
		query += " WHERE album_id = " + id_album;

	con.query(query + " ORDER BY (title)", (err, images) =>{
		if(err)
		{
			res.status(500).send({message: 'Error en la peticion'})
		}
		else
		{
			if(!image)
			{
				res.status(404).send({message: 'No encontrado ninguns imagen.'});
			}
			else
			{
				res.status(200).send(images);
			}
		}
	});
}

function saveImage(req, res)
{
	var params = req.body;
	var title = params.title;
	var picture = params.picture;
	var album_id = params.album_id;

	var new_image  = {title: title, picture: picture, album_id: album_id};	

	con.query(image.insert(), new_image, (err, result) =>{
		if(err)
		{
			res.status(500).send({message: 'Error al guardar el image.'})
		}
		else
		{
			if(!image)
			{
				res.status(404).send({message: 'No se ha guardado el image.'});
			}
			else
			{
				res.status(200).send(result);
			}
		}
	});

}

function setImage(req, res)
{
	var imageId = req.params.id;

	var params = req.body;
	// var title = params.title;
	// var picture = params.picture;

	// var new_image  = {title: title, picture: picture};	

	con.query(image.updateById2(imageId), params, (err, result) =>{
		if(err)
		{
			res.status(500).send({message: 'Error al actualizar la imagen. \n' + err })
		}
		else
		{
			if(!image)
			{
				res.status(404).send({message: 'No se ha actualizado el image.'});
			}
			else
			{
				res.status(200).send({result});
			}
		}
	});
}

function deleteImage(req, res)
{
	var imageId = req.params.id;

	con.query(image.deleteById(imageId), (err, image) =>{
		if(err)
		{
			res.status(500).send({message: "Error en la peticion \n" + err})
		}
		else
		{
			if(!image)
			{
				res.status(404).send({message: 'La imagen no se ha podido eliminar.'});
			}
			else
			{
				res.status(200).send({image});
			}
		}

	});
}

function uploadImage(req, res)
{
	var imageId = req.params.id;
	var file_name = 'no subido ...';

	if(req.files)
	{
		var file_path = req.files.image.path;
		var file_split = file_path.split('/');
		var file_name = file_split[1];
		
		var params = {picture: file_name};	

		con.query(image.updateById2(imageId), params, (err, result) =>{
			if(err)
			{
				res.status(500).send({message: 'Error al actualizar la imagen. \n' + err })
			}
			else
			{
				if(!image)
				{
					res.status(404).send({message: 'No se ha actualizado la imagen.'});
				}
				else
				{
					res.status(200).send({result});
				}
			}
		});
	}
	else
	{
		res.status(200).send({ result: "No ha subido ninguna imagen!!"});
	}
}

var fs = require('fs');
function getImageFile(req, res)
{
	var imageFile = req.params.imageFile;

	fs.exists('./uploads/'+imageFile, (exists)=>{
		if(exists)
			res.sendFile(path.resolve('./uploads/'+imageFile));
		else 
			res.status(200).send({message: 'No existe la imagen.'});

	});
}

module.exports = {
	getImage,
	getImages,
	saveImage,
	setImage,
	deleteImage,
	uploadImage,
	getImageFile
};