'use strict'

var album = require('../models/album');
var mysql = require('mysql');


var con = mysql.createConnection({
  host: "localhost",
  user: "pepe",
  password: "",
  database: "appalbum"
});


function getAlbum(req, res)
{
	var albumId = req.params.id;

	con.query(album.getById(albumId), (err, album) =>{
		if(err)
		{
			res.status(500).send({message: 'Error en la peticion'})
		}
		else
		{
			if(!album)
			{
				res.status(404).send({message: 'El album no exite.'});
			}
			else
			{
				res.status(200).send(album[0]);
			}
		}

	});
}

function getAlbums(req, res)
{
	con.query(album.getAlbums(), (err, albums) =>{
		if(err)
		{
			res.status(500).send({message: 'Error en la peticion'})
		}
		else
		{
			if(!album)
			{
				res.status(404).send({message: 'No encontrado ningun album.'});
			}
			else
			{
				res.status(200).send(albums);
			}
		}
	});
}

function saveAlbum(req, res)
{
	var params = req.body;
	var title = params.title;
	var description = params.description;

	var new_album  = {title: title, description: description};	

	con.query(album.insert(), new_album, (err, result) =>{
		if(err)
		{
			res.status(500).send({message: 'Error al guardar el album.'})
		}
		else
		{
			if(!album)
			{
				res.status(404).send({message: 'No se ha guardado el album.'});
			}
			else
			{
				res.status(200).send({result});
			}
		}
	});

}

function setAlbum(req, res)
{
	var albumId = req.params.id;

	var params = req.body;
	var title = params.title;
	var description = params.description;

	// var new_album  = {title: title, description: description};	

	con.query(album.updateById2(albumId), params, (err, result) =>{
		if(err)
		{
			res.status(500).send({message: 'Error al actualizar el album. \n' + err })
		}
		else
		{
			if(!album)
			{
				res.status(404).send({message: 'No se ha actualizado el album.'});
			}
			else
			{
				res.status(200).send({result});
			}
		}
	});
}

function deleteAlbum(req, res)
{
	var albumId = req.params.id;

	con.query(album.deleteById(albumId), (err, album) =>{
		if(err)
		{
			res.status(500).send({message: "Error en la peticion \n" + err})
		}
		else
		{
			if(!album)
			{
				res.status(404).send({message: 'El album no se ha podido eliminar.'});
			}
			else
			{
				res.status(200).send({album});
			}
		}

	});
}

module.exports = {
	getAlbum,
	getAlbums,
	saveAlbum,
	setAlbum,
	deleteAlbum
};