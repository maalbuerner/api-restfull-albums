'use strict'

class Album
{
	createTable()
	{
		return 'CREATE TABLE IF NOT EXISTS album (id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255), description VARCHAR(255)) ENGINE=INNODB;';
	}

	getAlbums()
	{
		return "SELECT * FROM album";
	}

	getById(id)
	{
		return `SELECT * FROM album WHERE id = ${id}`;	
	}

	insert()
	{
		return 'INSERT INTO album SET ?';
	}

	updateById(id)
	{
		return 'UPDATE album SET title = ?, description = ? WHERE id = '+ id;
	}

	updateById2(id)
	{
		return 'UPDATE album SET ? WHERE id = '+ id;
	}

	deleteById(id)
	{
		return `DELETE FROM album WHERE id = ${id}`;		
	}
}

// var mysql = require('mongoose');
// var schema = mysql.Schema;

// var AlbumSchema = schema({
// 	title: String,
// 	description: String
// });

// module.exports = mysql.model('album', AlbumSchema);
module.exports = new Album();