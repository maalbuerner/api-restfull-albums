'use strict'

class Image
{
	createTable()
	{
		return 'CREATE TABLE IF NOT EXISTS image ( id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255), picture VARCHAR(255), album_id INT, INDEX alb_ind (album_id), FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE ) ENGINE=INNODB;';
	}

	getImages()
	{
		return "SELECT * FROM image";
	}

	getById(id)
	{
		return `SELECT * FROM image WHERE id = ${id}`;	
	}

	insert()
	{
		return 'INSERT INTO image SET ?';
	}

	updateById(id)
	{
		return 'UPDATE image SET (title = ?, picture = ?) WHERE id = '+ id;
	}

	updateById2(id)
	{
		return 'UPDATE image SET ? WHERE id = '+ id;
	}

	deleteById(id)
	{
		return `DELETE FROM image WHERE id = ${id}`;		
	}
}

module.exports = new Image();



// var mysql = require('mysql');
// var schema = mysql.Schema;

// var ImageSchema = schema({
// 	title: String,
// 	picture: String,
// 	album: { type: Schema.ObjectId, ref: 'album' }
// });

// module.exports = mysql.model('image', ImageSchema);