'use strict'

var express = require('express');
var album_controller = require('../controllers/albumController');
var api = express.Router();

api.get('/album/:id', album_controller.getAlbum);
api.get('/albums', album_controller.getAlbums);
api.post('/albums', album_controller.saveAlbum);
api.put('/album/:id', album_controller.setAlbum);
api.delete('/album/:id', album_controller.deleteAlbum);

module.exports = api;