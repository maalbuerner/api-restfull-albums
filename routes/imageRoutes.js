'use strict'

var express = require('express');
var image_controller = require('../controllers/imageController');
var api = express.Router();

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart({ uploadDir: './uploads' })

api.get('/image/:id', image_controller.getImage);
api.get('/images/:id_album?', image_controller.getImages);
api.post('/images', image_controller.saveImage);
api.put('/image/:id', image_controller.setImage);
api.delete('/image/:id', image_controller.deleteImage);
api.post('/upload-image/:id', multipartMiddleware, image_controller.uploadImage);
api.get('/get-image/:imageFile', multipartMiddleware, image_controller.getImageFile);

module.exports = api;